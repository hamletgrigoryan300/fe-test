# fe-test

## Project setup
```
npm install
```

### To build the project
```
npm run build
```
Then just open index.html in the dist folder

### Run your end-to-end tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
