import Vue from 'vue'
import store from '@/store'
import router from '@/router'

let notify = alert

setTimeout(() => {
    notify = Vue.$toast.open
})

const axios = require('axios').create({
    baseURL: process.env.VUE_APP_API_URL,
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
    },
})

axios.interceptors.request.use(config => {
    let accessToken = store.state.auth.accessToken
    if (accessToken)
        config.headers['Authorization'] = `Bearer ${accessToken}`

    return config
})

axios.interceptors.response.use(res => res, e => {
    if (!e.response || !e.response.status)
        return Promise.reject(e)

    switch (e.response.status) {
        case 401:
            router.push({ name: 'login' })
            notify('Session expired')
            break
    }

    return Promise.reject(e)
})

Vue.prototype.$axios = axios

export default axios
