import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'

Vue.use(VueRouter)

const files = require.context('@/views', false, /\.vue$/)

/**
 * @property Home
 * @property Login
 * @property Logout
 * @property Reports
 * @property CreateReport
 * @property EditReport
 */
const c = {}
files.keys().map(key => {
    let name = key.slice(2, -4)
    c[name] = files(key).default
})

const routes = [
    {
        path: '/',
        name: 'home',
        component: c.Home,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/login',
        name: 'login',
        component: c.Login,
        meta: {
            guest: true,
        },
    },
    {
        path: '/logout',
        name: 'logout',
        component: c.Logout,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/reports',
        name: 'reports',
        component: c.Reports,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/reports/create',
        name: 'create-report',
        component: c.CreateReport,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/reports/edit',
        name: 'edit-report',
        component: c.EditReport,
        meta: {
            requiresAuth: true,
        },
    },
]

const router = new VueRouter({ routes })

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.getters['auth/loggedIn'])
            next({ name: 'login' })
        else
            next()
    }
    else if (to.matched.some(record => record.meta.guest) && store.getters['auth/loggedIn'])
        next('/')
    else
        next()
})

export default router
