import axios from '@/plugins/axios'

export default {
    state: {
        accessToken: null,
    },
    getters: {
        loggedIn: state => !!state.accessToken,
    },
    mutations: {
        setToken (state, token) {
            state.accessToken = token
        },
    },
    actions: {
        login ({ commit }, credentials) {
            return axios.post('api/login', credentials).then(({ data }) => {
                commit('setToken', data.token)
            })
        },
    },
    namespaced: true,
}
