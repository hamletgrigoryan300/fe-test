import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

import reports from './reports'
import auth from './auth'

export default new Vuex.Store({
    modules: {
        reports,
        auth,
    },
    plugins: [
        createPersistedState({
            paths: ['auth'],
        })
    ],
})
