import axios from '@/plugins/axios'
import Vue from 'vue'

const getDefaultSalesReport = () => ({
    report: {
        type: 'sales',
        name: '',
        status: 'draft',
    },
    sections: [
        {
            type: 'info',
            data: {
                name: '',
                area: '',
            },
            version: 1,
        },
        {
            type: 'customize',
            data: {},
        },
    ],
})

const getDefaultRentalReport = () => ({
    report: {
        type: 'rental',
        name: '',
        status: 'draft',
    },
    sections: [
        {
            type: 'info',
            data: {
                name: '',
                area: '',
                phone: '',
            },
            version: 2,
        },
        {
            type: 'search',
            data: {},
        },
        {
            type: 'customize',
            data: {},
        },
    ],
})

export default {
    state: {
        loading: false,
        reports: [],
        report: null,
        draft: null,
    },
    getters: {

    },
    mutations: {
        setLoading (state, val) {
            state.loading = val
        },
        setReports (state, reports) {
            state.reports = reports
        },
        setReport (state, report) {
            state.report = report
        },
        setDraft (state, draft) {
            state.draft = draft
        },
        setReportName (state, name) {
            state.report.report.name = name
        },
        createSalesReport (state) {
            state.report = getDefaultSalesReport()
        },
        createRentalReport (state) {
            state.report = getDefaultRentalReport()
        },
        switchType (state) {
            state.report = state.report.report.type === 'sales' ? getDefaultRentalReport() : getDefaultSalesReport()
        },
        setReportData (state, { sectionType, field, value }) {
            Vue.set(state.report.sections.find(section => section.type === sectionType).data, field, value)
        },
    },
    actions: {
        load ({ commit }) {
            commit('setLoading', true)
            return axios.get('api/v2/reports').then(({ data }) => {
                commit('setReports', data)
            }).finally(() => {
                commit('setLoading', false)
            })
        },
        createReport ({ commit, state }) {
            commit('setLoading', true)
            let report = state.report
            return axios.post('api/reports/' + report.report.type, report)
                .finally(() => {
                    commit('setLoading', false)
                    commit('setDraft', report)
                    commit('setReport', null)
                })
        },
    },
    namespaced: true,
}
