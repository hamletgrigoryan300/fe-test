describe('auth', () => {
  it('redirects to login page', () => {
    cy.visit('/')
    cy.url().should('include', 'login')

    cy.get('[data-cy=email]').type('admin@gmail.com')
    cy.get('[data-cy=password]').type('somepassword')
    cy.wait(1000)
    cy.contains('Submit').click()

    cy.url().should('include', 'reports')
  })

  it('logging out', () => {
    cy.login()

    cy.get('[data-cy=home]').click()

    cy.wait(1000)

    cy.get('[data-cy=logout]').click()

    cy.url().should('include', 'login')
  })
})
