describe('reports', () => {
    beforeEach(() => {
        cy.login()
    })

    it('shows report list', () => {
        cy.visit('/#/reports')
        cy.get('table tr').should('have.length.greaterThan', 2)
        cy.wait(1000)
    })

    it('opens create report page', () => {
        cy.visit('/#/reports')
        cy.contains('Create report').click()
        cy.url().should('include', 'reports/create')
        cy.contains('Create Rental Report')
        cy.contains('Create Sales Report').click()

        cy.get('input').should('have.length.greaterThan', 2)

        cy.get('input[placeholder=Name]').type('New Report')
        cy.get('input[placeholder=name]').type('info name')
        cy.get('input[placeholder=area]').type('info area')

        cy.contains('create').click()

        cy.url().should('include', 'reports')
    })
})