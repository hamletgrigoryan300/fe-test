Cypress.Commands.add('login', () => {
    cy.visit('/#/login')

    cy.get('[data-cy=email]').type('admin@gmail.com')
    cy.get('[data-cy=password]').type('somepassword')
    cy.contains('Submit').click()

    cy.url().should('include', 'reports')
})
